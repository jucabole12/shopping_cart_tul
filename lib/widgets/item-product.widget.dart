import 'package:flutter/material.dart';
import 'package:shopping_cart/data/model/product.model.dart';
import 'package:shopping_cart/widgets/add-button.widget.dart';

class ItemProductWidget extends StatelessWidget {
  const ItemProductWidget({this.product});
  final ProductModel product;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8.0,
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Image.network(
                product.image,
                fit: BoxFit.contain,
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  Text(product.name,
                      style: TextStyle(color: Color(0xff005853))),
                  SizedBox(height: 10),
                  Text(
                    product.description,
                    style: Theme.of(context)
                        .textTheme
                        .overline
                        .copyWith(color: Colors.grey),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 10),
                  Text('\$${product.price}'),
                ],
              ),
            ),
            AddButtonWidget(item: product),
          ],
        ),
      ),
    );
  }
}
