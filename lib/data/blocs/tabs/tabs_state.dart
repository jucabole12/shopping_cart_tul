import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class TabsState extends Equatable {
  TabsState();
}

class CurrentIndexChanged extends TabsState {
  final int currentIndex;

  CurrentIndexChanged({@required this.currentIndex});

  @override
  String toString() => 'CurrentIndexChanged to $currentIndex';

  @override
  List<Object> get props => [];
}

class PageLoading extends TabsState {
  @override
  String toString() => 'PageLoading';

  @override
  List<Object> get props => [];
}

class FirstPageLoaded extends TabsState {
  final String text;

  FirstPageLoaded({@required this.text});

  @override
  String toString() => 'FirstPageLoaded with text: $text';

  @override
  List<Object> get props => [];
}

class SecondPageLoaded extends TabsState {
  final int number;

  SecondPageLoaded({@required this.number});

  @override
  String toString() => 'SecondPageLoaded with number: $number';

  @override
  List<Object> get props => [];
}
