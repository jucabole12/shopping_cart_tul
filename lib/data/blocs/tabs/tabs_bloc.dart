import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:shopping_cart/data/blocs/tabs/tabs_event.dart';
import 'package:shopping_cart/data/blocs/tabs/tabs_state.dart';
import 'package:shopping_cart/pages/catalog/catalog_repository.dart';
import 'package:shopping_cart/pages/history_cart/history_cart_repository.dart';

class TabsBloc extends Bloc<TabsEvent, TabsState> {
  final CatalogRepository firstPageRepository;
  final HistoryCartRepository secondPageRepository;
  int currentIndex = 0;

  TabsBloc(this.firstPageRepository, this.secondPageRepository)
      : assert(firstPageRepository != null),
        assert(secondPageRepository != null),
        super(null);

  TabsState get initialState => PageLoading();

  @override
  Stream<TabsState> mapEventToState(TabsEvent event) async* {
    if (event is AppStarted) {
      add(PageTapped(index: currentIndex));
    }
    if (event is PageTapped) {
      currentIndex = event.index;
      yield CurrentIndexChanged(currentIndex: currentIndex);
      yield PageLoading();

      if (currentIndex == 0) {
        var data = await _getFirstPageData();
        yield FirstPageLoaded(text: data);
      }
      if (currentIndex == 1) {
        var data = await _getSecondPageData();
        yield SecondPageLoaded(number: data);
      }
    }
  }

  Future<String> _getFirstPageData() async {
    var data = firstPageRepository.data;
    if (data == null) {
      await firstPageRepository.fetchData();
      data = firstPageRepository.data;
    }
    return data;
  }

  Future<int> _getSecondPageData() async {
    var data = secondPageRepository.data;
    if (data == null) {
      await secondPageRepository.fetchData();
      data = secondPageRepository.data;
    }
    return data;
  }
}
