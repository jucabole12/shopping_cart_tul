class ProductModel {
  final String name;
  final String sku;
  final String description;
  final double price;
  final String image;
  final int id;

  ProductModel({
    this.name,
    this.sku,
    this.description,
    this.price,
    this.image,
    this.id,
  });

  Map<String, dynamic> toJSON() => {
        'id': null,
        'name': name,
        'sku': sku,
        'description': description,
        'price': price,
        'image': image,
      };

  ProductModel fromJson(Map<String, dynamic> json) {
    return ProductModel(
      name: json['name'],
      sku: json['sku'],
      description: json['description'],
      image: json['image'],
    );
  }

  List<ProductModel> toListFromJson(List<Map<String, dynamic>> str) {
    return List<ProductModel>.from(str.map((x) => ProductModel().fromJson(x)));
  }
}
