// import 'package:shopping_cart/data/model/product.model.dart';
// import 'package:shopping_cart/data/model/product_cart.model.dart';
// import 'package:shopping_cart/utils/bloc/bloc_field.dart';
// import 'package:shopping_cart/utils/bloc/bloc_provider.dart';
// import 'package:shopping_cart/utils/utils.dart';

// class HomeBloc implements BlocBase {

//   static HomeBloc _instance;
//   factory HomeBloc() {
//     if(_instance == null){
//       _instance = new HomeBloc._internal();
//       _instance.init();
//     }
//     return _instance;
//   }
//   HomeBloc._internal();

//   init() {
//     productsList.sink(getFakeProducts);
//   }

//   final indexSelectedTab = FieldBlocGeneric<int>(defaultValue: 1);
//   final productsList = FieldBlocGeneric<List<ProductModel>>();
//   final productListSelected = FieldBlocGeneric<List<ProductModel>>();

//   setProducListSelected(ProductModel product){
//     // ProductCartModel productCart = ProductCartModel(
//     //   cartId: 0,
//     //   productId: product.id,
//     //   quantity: 1,
//     // );
//     // List<ProductCartModel> listProducts = productListSelected.value ?? [];
//     // listProducts.add(productCart);
//     // productListSelected.sink(listProducts);
//     // print(productListSelected.value);
//     List<ProductModel> listProducts = productListSelected.value ?? [];
//     listProducts.add(product);
//     productListSelected.sink(listProducts);
//   }

//   deleteItemToSelectedList(ProductModel product){
//     List<ProductModel> listProducts = productListSelected.value ;
//     listProducts.remove(product);
//     productListSelected.sink(listProducts);
//   }

//   @override
//   void dispose() {
//     indexSelectedTab.dispose();
//     productsList.dispose();
//     productListSelected.dispose();
//   }
// }

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/data/model/catalog.dart';
import 'package:shopping_cart/shopping_repository.dart';

part 'catalog_event.dart';
part 'catalog_state.dart';

class CatalogBloc extends Bloc<CatalogEvent, CatalogState> {
  CatalogBloc({@required this.shoppingRepository}) : super(CatalogLoading());

  final ShoppingRepository shoppingRepository;

  @override
  Stream<CatalogState> mapEventToState(
    CatalogEvent event,
  ) async* {
    if (event is CatalogStarted) {
      yield* _mapCatalogStartedToState();
    }
  }

  Stream<CatalogState> _mapCatalogStartedToState() async* {
    yield CatalogLoading();
    try {
      final catalog = await shoppingRepository.loadCatalog();
      yield CatalogLoaded(Catalog(itemNames: catalog));
    } catch (_) {
      yield CatalogError();
    }
  }
}
