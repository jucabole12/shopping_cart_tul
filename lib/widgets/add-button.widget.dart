import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart/data/blocs/cart/cart_bloc.dart';
import 'package:shopping_cart/data/blocs/counter_cubit.dart';
import 'package:shopping_cart/data/model/product.model.dart';

class AddButtonWidget extends StatelessWidget {
  const AddButtonWidget({Key key, @required this.item}) : super(key: key);

  final ProductModel item;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        if (state is CartLoading) {
          return const CircularProgressIndicator();
        }
        if (state is CartLoaded) {
          return InkWell(
            onTap: () {
              if (!state.cart.contains(item)) {
                context.read<CartBloc>().add(CartItemAdded(item));
                context.read<CounterCubit>().increment();
              } else {
                context.read<CartBloc>().add(RemoveCartItem(item));
                context.read<CounterCubit>().decrement();
              }
            },
            child: (!state.cart.contains(item))
                ? Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                            begin: Alignment.centerRight,
                            end: Alignment.centerLeft,
                            colors: [
                              Color(0xff005853),
                              Color(0xff3bb2b8),
                            ])),
                    child: Padding(
                      padding: EdgeInsets.all(4.0),
                      child: Text(
                        'Agregar',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                            begin: Alignment.centerRight,
                            end: Alignment.centerLeft,
                            colors: [
                              Colors.red,
                              Colors.red[900],
                            ])),
                    child: Padding(
                      padding: EdgeInsets.all(4.0),
                      child: Text(
                        'Eliminar',
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
          );
        }
        return const Text('Something went wrong!');
      },
    );
  }
}
