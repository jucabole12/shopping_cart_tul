import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart/data/blocs/cart/cart_bloc.dart';
import 'package:shopping_cart/data/blocs/catalog/catalog_bloc.dart';
import 'package:shopping_cart/data/blocs/tabs/tabs_bloc.dart';
import 'package:shopping_cart/pages/catalog/catalog_repository.dart';
import 'package:shopping_cart/pages/history_cart/history_cart_repository.dart';
import 'package:shopping_cart/pages/home/home.page.dart';
import 'package:shopping_cart/shopping_repository.dart';

import 'data/blocs/counter_cubit.dart';

class App extends StatelessWidget {
  const App({Key key, @required this.shoppingRepository}) : super(key: key);

  final ShoppingRepository shoppingRepository;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CatalogBloc>(
          create: (_) => CatalogBloc(
            shoppingRepository: shoppingRepository,
          )..add(CatalogStarted()),
        ),
        BlocProvider<CartBloc>(
          create: (_) => CartBloc(
            shoppingRepository: shoppingRepository,
          )..add(CartStarted()),
        ),
        BlocProvider<CounterCubit>(
          create: (_) => CounterCubit(),
          child: HomePage(),
        ),
        BlocProvider<TabsBloc>(
          create: (context) => TabsBloc(
            CatalogRepository(),
            HistoryCartRepository(),
          ),
          child: HomePage(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Bloc Shopping Cart',
        initialRoute: '/',
        routes: {
          '/': (_) => HomePage(),
          // '/cart': (_) => CartPage(),
        },
      ),
    );
  }
}
