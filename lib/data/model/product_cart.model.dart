class ProductCartModel {
  final int cartId;
  final int productId;

  ProductCartModel({this.cartId, this.productId});
}
