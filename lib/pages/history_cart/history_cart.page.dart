import 'package:flutter/material.dart';

class HistoryCartPage extends StatelessWidget {
  HistoryCartPage();

  final cartsFinished = [
    {
      'state': true,
      'products': 'Taladro Bosch GSB,Pulidora alámbrica',
    },
    {
      'state': true,
      'products': 'Hidrolavadora Karcher,Set de destornilladores',
    },
    {
      'state': false,
      'products':
          'Caladora 720W,Set de herramientas,Combo pulidora & taladro Stanley',
    },
    {
      'state': false,
      'products':
          'Caladora 720W,Set de herramientas,Combo pulidora & taladro Stanley',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      width: double.infinity,
      height: double.infinity,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: cartsFinished.length,
        itemBuilder: (context, index) {
          return Card(
            elevation: 5,
            color: cartsFinished[index]['state']
                ? Colors.green.withOpacity(0.5)
                : Colors.red.withOpacity(0.5),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(
                    'Venta #${index + 1}',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  returnCartList(cartsFinished[index]['products']),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget returnCartList(String cart) {
    var listToReturn = cart.split(',');
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: listToReturn.length,
      itemBuilder: (context, index) {
        return ListTile(
          leading: Icon(
            Icons.fiber_manual_record,
            size: 15,
          ),
          title: Text(listToReturn[index]),
        );
      },
    );
  }
}
