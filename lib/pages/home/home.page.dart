import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart/data/blocs/counter_cubit.dart';
import 'package:shopping_cart/data/blocs/tabs/tabs_bloc.dart';
import 'package:shopping_cart/data/blocs/tabs/tabs_event.dart';
import 'package:shopping_cart/data/blocs/tabs/tabs_state.dart';
import 'package:shopping_cart/pages/history_cart/history_cart.page.dart';
import 'package:shopping_cart/pages/catalog/catalog.page.dart';
import 'package:shopping_cart/widgets/drawer.widget.dart';

class HomePage extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final bottomNavigationBloc = BlocProvider.of<TabsBloc>(context);
    return Scaffold(
      key: _scaffoldKey,
      drawerEnableOpenDragGesture: false,
      endDrawer: Container(child: DrawerWidget()),
      appBar: AppBar(
        backgroundColor: Colors.yellow[700],
        actions: [
          IconButton(
            icon: BlocBuilder<CounterCubit, int>(
              builder: (context, state) {
                if (state != 0) {
                  return Stack(
                    children: [
                      Icon(Icons.shopping_cart),
                      Positioned(
                        right: 0,
                        top: 0,
                        child: Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 14,
                            minHeight: 14,
                          ),
                          child: Text(
                            '$state',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 8,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  );
                }
                return Icon(Icons.shopping_cart);
              },
            ),
            onPressed: () {
              _scaffoldKey.currentState.openEndDrawer();
            },
          )
        ],
        title: Row(
          children: [
            Container(
              padding: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/tul.png'),
              ),
            ),
            Text('Carrito de compras Tul')
          ],
        ),
      ),
      body: createBodyByTab(),
      bottomNavigationBar: BlocBuilder<TabsBloc, TabsState>(
          builder: (BuildContext context, TabsState state) {
        return BottomNavigationBar(
          currentIndex: bottomNavigationBloc.currentIndex,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt),
              label: 'Catálogo',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.my_library_books_outlined),
              label: 'Historial',
            ),
          ],
          onTap: (index) => bottomNavigationBloc.add(PageTapped(index: index)),
        );
      }),
    );
  }

  Widget createBodyByTab() {
    var indexPage = 0;
    final widgetsNavigationBottom = <Widget>[
      CatalogPage(),
      HistoryCartPage(),
    ];
    return BlocBuilder<TabsBloc, TabsState>(
      builder: (BuildContext context, TabsState state) {
        if (state is PageLoading) {
          return Center(child: CircularProgressIndicator());
        }
        if (state is FirstPageLoaded) {
          indexPage = 0;
        }
        if (state is SecondPageLoaded) {
          indexPage = 1;
        }
        return IndexedStack(
          index: indexPage,
          children: widgetsNavigationBottom,
        );
      },
    );
  }
}
