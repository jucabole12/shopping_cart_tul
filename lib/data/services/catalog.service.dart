import 'dart:convert';

import 'package:shopping_cart/data/model/product.model.dart';
import 'package:shopping_cart/utils/api.service.dart';
import 'package:shopping_cart/utils/server.response.dart';

class CatalogService {
  static CatalogService _instance;
  final ApiService _apiService = ApiService();

  factory CatalogService() {
    _instance ??= CatalogService._internal();
    return _instance;
  }
  CatalogService._internal();

  Future<ResponseApi> getProductList() async {
    final response = await _apiService.getApi(api: '');
    if (response.isSuccess) {
      List<Map<String, dynamic>> odataResult = json.decode(response.result);
      dynamic newlistEmployees;
      try {
        newlistEmployees = ProductModel().toListFromJson(odataResult);
      } catch (e) {
        print(e.toString());
      }
      return ResponseApi(
        isSuccess: true,
        result: newlistEmployees,
        message: 'Ok',
      );
    }
    return ResponseApi(
      isSuccess: false,
      result: null,
      message: 'Error',
    );
  }

  void dispose() {
    _instance = null;
  }
}
