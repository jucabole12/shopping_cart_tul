import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/data/model/product.model.dart';

class Catalog extends Equatable {
  Catalog({@required this.itemNames});

  final List<ProductModel> itemNames;

  ProductModel getById(int id) => itemNames[id];

  ProductModel getByPosition(int position) => getById(position);

  @override
  List<Object> get props => [itemNames];
}
