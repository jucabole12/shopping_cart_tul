import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/shopping_repository.dart';
import 'package:shopping_cart/simple_bloc_observer.dart';

import 'app.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(App(shoppingRepository: ShoppingRepository()));
}

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Shopping cart',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       initialRoute: RoutesName.home,
//       routes: Routes.loadRoutes,
//     );
//   }
// }