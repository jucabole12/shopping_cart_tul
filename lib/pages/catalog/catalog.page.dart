import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart/data/blocs/catalog/catalog_bloc.dart';
import 'package:shopping_cart/widgets/item-product.widget.dart';

class CatalogPage extends StatelessWidget {
  const CatalogPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: BlocBuilder<CatalogBloc, CatalogState>(
        builder: (context, state) {
          if (state is CatalogLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is CatalogLoaded) {
            return GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 2 / 3,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                itemCount: 5,
                padding: EdgeInsets.all(10.0),
                itemBuilder: (context, index) {
                  return ItemProductWidget(
                      product: state.catalog.getByPosition(index));
                });
          }
          return Text('Something went wrong!');
        },
      ),
    );
  }
}
