part of 'cart_bloc.dart';

@immutable
abstract class CartState extends Equatable {
  const CartState();
}

class CartLoading extends CartState {
  @override
  List<Object> get props => [];
}

class CartLoaded extends CartState {
  CartLoaded({this.cart});

  final List<ProductModel> cart;

  @override
  List<Object> get props => cart;
}

class CartError extends CartState {
  @override
  List<Object> get props => [];
}
