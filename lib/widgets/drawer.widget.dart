import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:shopping_cart/data/blocs/cart/cart_bloc.dart';
import 'package:shopping_cart/data/blocs/counter_cubit.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
          if (state is CartLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is CartLoaded) {
            if (state.cart.isNotEmpty) {
              return Column(
                children: [
                  Expanded(
                    flex: 4,
                    child: ListView.builder(
                        itemCount: state.cart.length,
                        itemBuilder: (context, index) {
                          return Card(
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Image.network(
                                    state.cart[index].image,
                                    width: 70,
                                  )),
                                  Expanded(
                                    flex: 4,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 10.0),
                                      child: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(state.cart[index].name,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                              InkWell(
                                                onTap: () {
                                                  context.read<CartBloc>().add(
                                                        RemoveCartItem(
                                                            state.cart[index]),
                                                      );
                                                  context
                                                      .read<CounterCubit>()
                                                      .decrement();
                                                  // bloc.deleteItemToSelectedList(
                                                  //     snapshot.data[index]);
                                                },
                                                child: Icon(Icons.delete,
                                                    color: Colors.red),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                state.cart[index].price
                                                    .toString(),
                                              ),
                                              SizedBox(width: 5),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 15.0),
                    child: ElevatedButton(
                      style:
                          ElevatedButton.styleFrom(primary: Color(0xff005853)),
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.check),
                          Text('Realizar compra'),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Lottie.asset('assets/sad.json', height: 100),
                  SizedBox(height: 10),
                  Center(
                    child: Text(
                      'No haz agregado productos al carrito',
                      style: TextStyle(color: Color(0xff005853), fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.only(top: 8.0, right: 60, left: 60),
                    child: ElevatedButton(
                      style:
                          ElevatedButton.styleFrom(primary: Color(0xff005853)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Ir a comprar'),
                    ),
                  )
                ],
              );
            }
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        }),
      ),
    );
  }
}
