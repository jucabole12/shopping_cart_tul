import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class TabsEvent extends Equatable {
  TabsEvent();
}

class AppStarted extends TabsEvent {
  @override
  String toString() => 'AppStarted';

  @override
  List<Object> get props => [];
}

class PageTapped extends TabsEvent {
  final int index;

  PageTapped({@required this.index});

  @override
  String toString() => 'PageTapped: $index';

  @override
  List<Object> get props => [];
}
