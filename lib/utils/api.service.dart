import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shopping_cart/utils/server.response.dart';

class ApiService {
  static ApiService _instance;

  factory ApiService() {
    _instance ??= ApiService._internal();
    return _instance;
  }

  ApiService._internal();

  Map<String, String> getHeaderContentType() {
    var headers = <String, String>{
      'Content-type': 'application/json',
    };
    return headers;
  }

  Future<ResponseApi> getApi({@required String api}) async {
    var client = http.Client();
    final url = Uri.decodeComponent(
        'https://delivery-tul-default-rtdb.firebaseio.com/products.json');
    try {
      var response = await client.get(Uri.parse(url));
      final responseStatusCode =
          ResponseApi.getResponseStatusCode(statusCode: response.statusCode);
      if (!responseStatusCode.isSuccess) {
        return ResponseApi(isSuccess: false, message: response.body.toString());
      }
      return ResponseApi(
          result: response.body,
          isSuccess: true,
          message: 'Ok',
          headers: response.headers);
    } catch (e) {
      return ResponseApi(
          isSuccess: false,
          result: e,
          message: 'Response Error : ${e.toString()}');
    } finally {
      client.close();
    }
  }
}
