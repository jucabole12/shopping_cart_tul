import 'dart:async';

import 'data/model/product.model.dart';

const _delay = Duration(milliseconds: 800);

final List<ProductModel> catalog = [
  ProductModel(
    id: 1,
    name: 'Taladro Bosch GSB',
    description: 'Percutor 450W 3/8',
    image:
        'https://www.bosch-professional.com/binary/ocsmedia/optimized/750x422/o333237v54_gsb_16_re_dyn.png',
    sku: 'adas8768',
    price: 160000,
  ),
  ProductModel(
    id: 2,
    name: 'Pulidora alámbrica',
    description: 'Bosch 4-1/2\"',
    image:
        'https://ftmapp-production.s3.amazonaws.com/uploads/product/picture/1762/large_retina_GWS6115.png',
    sku: 'adas8768',
    price: 120000,
  ),
  ProductModel(
    id: 3,
    name: 'Hidrolavadora Karcher',
    description: 'P65547 1800PSI 1600W',
    image:
        'https://www.machenaonline.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/2/_/2_35_56.png',
    sku: 'adas8768',
    price: 800000,
  ),
  ProductModel(
    id: 4,
    name: 'Set de destornilladores',
    description: 'Yinsan 120 en 1 juego',
    image:
        'https://i.pinimg.com/originals/e9/9f/25/e99f256b6759712f71b8b2726ed2ab79.png',
    sku: 'adas8768',
    price: 100000,
  ),
  ProductModel(
    id: 5,
    name: 'Caladora 720W',
    description: 'Bosch 4350CT',
    image:
        'http://www.herramientasacz.com.mx/image/cache/data/ELECTRICO/B1516-500x500.png',
    sku: 'adas8768',
    price: 160000,
  ),
  ProductModel(
    id: 6,
    name: 'Set de herramientas',
    description: 'Reparación para uso doméstico Fusanmo',
    image:
        'https://ae01.alicdn.com/kf/H67decb69ef344c5db78b4bda1dc4154bJ/Juego-de-herramientas-de-reparaci-n-para-uso-dom-stico-caja-de-Herramienta-de-combinaci-n.jpg_q50.jpg',
    sku: 'adas8768',
    price: 35000,
  ),
  ProductModel(
    id: 7,
    name: 'Combo pulidora & taladro Stanley',
    description: 'Pulidora 600W y Taladro 700w',
    image:
        'https://belltec.com.co/8244-large_default/kit-pulidora-600w-taladro-700-acces-shg6070ka-b3-.jpg',
    sku: 'adas8768',
    price: 300000,
  ),
];

class ShoppingRepository {
  final _items = <ProductModel>[];

  Future<List<ProductModel>> loadCatalog() =>
      Future.delayed(_delay, () => catalog);

  Future<List<ProductModel>> loadCartItems() =>
      Future.delayed(_delay, () => _items);

  void addItemToCart(ProductModel item) => _items.add(item);

  void removeItemToCart(ProductModel item) => _items.remove(item);
}
