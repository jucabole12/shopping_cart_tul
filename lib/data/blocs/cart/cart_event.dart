part of 'cart_bloc.dart';

@immutable
abstract class CartEvent extends Equatable {
  const CartEvent();
}

class CartStarted extends CartEvent {
  @override
  List<Object> get props => [];
}

class CartItemAdded extends CartEvent {
  const CartItemAdded(this.item);
  final ProductModel item;

  @override
  List<Object> get props => [item];
}

class RemoveCartItem extends CartEvent {
  final ProductModel item;

  RemoveCartItem(this.item);

  @override
  List<Object> get props => [item];
}
