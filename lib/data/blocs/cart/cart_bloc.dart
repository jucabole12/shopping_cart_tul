import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:shopping_cart/data/model/product.model.dart';
import 'package:shopping_cart/shopping_repository.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc({@required this.shoppingRepository}) : super(CartLoading());

  final ShoppingRepository shoppingRepository;

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is CartStarted) {
      yield* _mapCartStartedToState();
    } else if (event is CartItemAdded) {
      yield* _mapCartItemAddedToState(event, state);
    } else if (event is RemoveCartItem) {
      yield* _mapCartItemRemovedToState(event, state);
    }
  }

  Stream<CartState> _mapCartStartedToState() async* {
    yield CartLoading();
    try {
      final items = await shoppingRepository.loadCartItems();
      yield CartLoaded(cart: items);
    } catch (_) {
      yield CartError();
    }
  }

  Stream<CartState> _mapCartItemAddedToState(
    CartItemAdded event,
    CartState state,
  ) async* {
    if (state is CartLoaded) {
      try {
        shoppingRepository.addItemToCart(event.item);
        yield CartLoaded(cart: state.cart);
      } on Exception {
        yield CartError();
      }
    }
  }

  Stream<CartState> _mapCartItemRemovedToState(
    RemoveCartItem event,
    CartState state,
  ) async* {
    if (state is CartLoaded) {
      try {
        shoppingRepository.removeItemToCart(event.item);
        yield CartLoaded(cart: state.cart);
      } on Exception {
        yield CartError();
      }
    }
  }
}
